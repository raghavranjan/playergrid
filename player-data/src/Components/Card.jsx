

const Card = ({player}) => {

    
    return ( 
        
        <div className="card" style={{width: "16rem", height:"18.5rem"}}>
     <div className='row'>       
  <div className="col"><img src={`/player-images/${player.Id}.jpg`} className="card-img-top" alt="..." style={{height:"6rem", width:"6rem"}}/></div>
  <div className='col'>
      <div className="badge bg-primary ">Value:  ${player.Value} </div>
  <div className="badge bg-primary ">
      <div className="accordion"></div>
    Next Match:<br/>{player.CCode}-{player.VsCCode}</div>
  
  </div>
  </div>
  <div className="container"><div className="card-body">
    <div className="col">
        <div className="row"><h5 className="card-title">Name: {player.PFName}</h5></div>
        <div className="row"><h5 className="card-title">Team: {player.TName}</h5></div>
        <div className="row"><p>Skill: {player.SkillDesc}</p></div>
        {player.VsCCode ? <div className="row"><p className="card-text">Next Match Time:<br/>
        <span className="time1">{player.MDate}</span></p>
        </div> : <div><p className="card-text">No upcoming Matches</p></div>}


        
    </div>
    </div>
    
  </div>
</div>
     );
}
 
export default Card;


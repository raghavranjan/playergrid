
import React, { Component } from 'react';
import Card from "./Card";
import axios from 'axios'

class CardGrid extends Component {
    state = {
        initialData : [],
        originalData : [],
        isOpen : false,
        teams : [],
        search : ''
    }
     componentDidMount() {
         let arr = [], teamarr =[]
        
         axios.get('https://api.npoint.io/20c1afef1661881ddc9c')
         .then((playersData)=>{
             //console.log(playersData.data)
             playersData.data.playerList.map(player =>{

                let aPlayer ={}
                aPlayer.Id=player.Id
                aPlayer.TID=player.TID
                aPlayer.PFName = player.PFName
                aPlayer.TName = player.TName
                aPlayer.SkillDesc = player.SkillDesc
                aPlayer.Value = player.Value
                aPlayer.Skill = player.Skill
                aPlayer.CCode = player.UpComingMatchesList[0].CCode
                aPlayer.VsCCode = player.UpComingMatchesList[0].VsCCode
                
                aPlayer.MDate=this.setTime(player.UpComingMatchesList[0].MDate)
                arr.push(aPlayer)
             })
             playersData.data.teamsList.map(team => {
                 let newTeam = {}
                 newTeam.TID = team.TID
                 newTeam.name = team.OfficialName
                 teamarr.push(newTeam)
                })
                arr.sort((a,b)=>{
                    if(parseFloat(a.Value) < parseFloat(b.Value))
                    return -1
                    else if (parseFloat(a.Value )> parseFloat(b.Value))
                    return 1;
                    else
                    return 0;
                })
            this.setState({teams:teamarr})
            this.setState({initialData:arr})
             this.setState({originalData:arr})
             //console.log(this.state.teams)
         })
         .catch((error)=>{
             alert('Some error Occoured')
             console.log('Error: ',error)
         })
    }
    
     setTime = (utcDate) => {
      const zone=[]
      const str = utcDate + ' UTC';
      const date = new Date(str)
      //console.log(utcDate,date.toLocaleString(), date)
      if(date!='Invalid Date'){
      const timezone = date.toString().split('(')[1]
      timezone.split(' ').forEach(item => {zone.push(item[0])})}
      return date.toLocaleString()+' '+zone.join('')
      }
    handleSubmit =(e)=>{
        e.preventDefault();
    }

    handleChange = (e) => {
        e.preventDefault();
        this.setState({search:e.currentTarget.value})
        //console.log(e.currentTarget.value)
        if(e.currentTarget.value.length== 0)
        this.setState({originalData:this.state.initialData})

        let searching = e.currentTarget.value.toLowerCase().trim();
        let data = this.state.initialData;
        //console.log(this.state.initialData)
        if(e.currentTarget.value.length > 1){
        let filteredData = data.filter(player=>{
            const [fname,lname=''] = player.PFName.split(' ')
            //console.log(searching,fname,lname)
            if(lname.toLowerCase().includes(searching) == true || 
            fname.toLowerCase().includes(searching) == true || player.TName.toLowerCase().includes(searching))
            return true;
        })

        this.setState({originalData:filteredData}) }
    }
    render() { 
        
        return (<>

        <nav className="navbar navbar-expand-lg navbar-dark bg-dark sticky-top " style={{width:"100%"}}>
        <div className="container-fluid">
            <div className="navbar-brand">Players List</div>
            
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav me-auto">
                <li className="nav-item">
                </li>
            </ul>
            
            <form className="d-flex" onSubmit={this.handleSubmit}>
                <input className="form-control search" value={this.state.search} name="search" onChange={this.handleChange}
                type="search" placeholder="Search by Player or Team..." aria-label="Search"></input>
            </form>
            </div>
        </div>
        </nav>
            

        <div className="container">
         <div className="card-grid">
            {this.state.originalData.map(player => {
                return <Card  key={player.Id} player={player}></Card>
            })}
        </div></div>
        
        </>); 
    }
}
 
export default CardGrid;

